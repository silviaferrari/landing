---
title: Exploring Hybrid Profiles in Design
layout: "page"
order: 04
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/da58386292b1-IMG_4702_S.jpg)

### Faculty
Oscar Tomico

### Assistant
[Sara Gonzalez de Ubieta](http://www.deubieta.com)  
From architecture to shoe making and material experimentation.  
[Ariel Guersenzvaig](http://www.elisava.net/en/center/professorate/ariel-guersenzvaig)  
From usability consultant to design researcher on ethical digital transformation.  
[Domestic Data Streamers](https://domesticstreamers.com/)  
With Pol Trias and Marta Handenawer. From product design to data enabled communication.  

### Syllabus and Learning Objectives
Research  has  shown  that  most  of  the  jobs  opportunities  and  future  challenges  that  will  arise  in  the  next  years  still  don’t  exist.  Instead  of  seeing  it  as  a  thread,  we  want  you  to  look  at  it  as  an  opportunity.  An  opportunity  to  actively  create  your  own  path,  your  own  vision  and  identity  rather  than  passively  wait  for  what  it  is  needed.  
In  MDEF  we  believe  that  learning  should  be  driven  by  your  motivations  and  not  by  our  (the  teachers)  thoughts.  We  want  you  to  be  in  control  of  your  own  development  specially  in  a  master  program  full  of  activities.  
In  this  course,  critical  reflection  will  help  you  to  map  your  strengths  and  weaknesses.  Group  discussions  will  make  you  aware  about  how  your  thinking,  interests  and  values  differ  from  others.  By  means  of  a  series  of  visits  to  key  professionals,  that  have  undergone  a  shift  in  their  careers,  we  want  you  to  plan  a  strategic  turn  for  yourself.  We  will  provide  you  with  a  variety  of  knowledge,  skills  and  attitudes  to  compare  yourself  with.  
At  the  end  of  this  course  we  expect  you  to  understand  who  you  are  and  what  makes  you  unique  (identity),  have  created  a  personal  “vision”  of  your  future  as  a  professional,  and  a  draft  development  plan  on  how  to  achieve  it. 

### Total Duration
Classes: 3-5 p.m, Mon-Fri  
Student work hours: 20 per week  
Total hours per student: 35 

### Structure and Phases
*Homework  (bootcamp): Finalize  or  improve  your  post  on  your  personal  web.*  
· Present  yourself  (your  identity  in  relation  to  your  strengths  and  weaknesses).  
· Where  would  you  be  in  10  years  (vision  on  design  and  your  future  role).  
· What  do  you  want  to  get  out  of  the  master  program  (personal  development  plan,  what  you  want  to  accomplish  during  this  year).   
*Day 1 to 3: Visit  and  reflect  on  the  hybrid  profiles  presented  (points  of  convergence  and  divergence).*  
Each  visit  will  consist  of  the  following:   
· Introduction  to  their  professional  practice  and  where  they  work  (university,  study,  company,  ...)  
· Background  (how  did  they  get  where  they  are  now)  
· Practical  exercise  on  a  topic  that  is  key  for  them  
· Reflection  with  MDEF  students  about  similarities  and  differences  in  their  understanding  of  what  design  is,  the  role  of  technology  and  value  for  society.      
*Day 4: Vision  and  Identity  class,  tools  and  techniques  for  self-reflection.*   
*Day 5: Personal  development  plan  class,  classes  and  FMP.*   

### Output
In this course personal and group reflections are key, that is why we expect you to deliver a series of notes and conclusions from each activity. We want you to post them in your personal blog daily so other students can see them too. The final result should be a text relating your current identity as a designer, your vision of the future, and a personal development plan for the master (and beyond). 

### Grading Method
Final document: 70%  
Daily reflections: 30%  

### Bibliography
[Annotated portfolios](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge)      
Schön, D. (1983). The Reflective Practitioner: How professionals think in action. London: Temple Smith  
[The  reflective  transformative  design  process.](https://www.researchgate.net/publication/221518234_The_reflective_transformative_design_process)    
Hummels, C. C. M., & Frens, J. W. (2009). In  CHI  2009  -  digital  life,  new  world:  conference  proceedings  and  extended  abstracts;  the  27th  Annual  CHI  Conference  on  Human  Factors  in  Computing  Systems,  April  4  -  9,  2009  in  Boston,  USA  (pp.  2655-2658).  New  York:  Association  for  Computing  Machinery,  Inc.    
[Designing  for  the  unknown:  A  design  process  for  the  future  generation  of  highly  interactive  systems  and  products.](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products)    
Hummels, C. and Frens, J. (2008). Proceedings  Conference  on  EPDE,  Barcelona,  Spain,  4-5  September  2008,  pp.  204-209.    
[Eindhoven designs volume 2: Developing the competence of designing intelligent systems.](https://pdfs.semanticscholar.org/db8b/edd527a469068c2b4ee78f505c6e2a5f0cbd.pdf)    
Hummels, C. and Vinke, D. (2009). Eindhoven University of Technology. 

### OSCAR TOMICO
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6cf613d0f7fa-IMG_9099_Edit_S.jpg)

### Email Address
<otomico@elisava.net>

### Personal Website
[Oscar Tomico - Elisava](http://www.elisava.net/en/center/professorate/oscar-tomico-plasencia)

### Twitter Account
@otomico

### Professional BIO
Oscar Tomico holds an MSc degree in Industrial Engineering from Polytechnic University of Catalonia (Spain) and a PhD from the same institution, awarded in 2007 with Cum Laude. During his research into Innovation Processes in Product Design, he investigated subjective experience-gathering techniques based on constructivist psychology. After finishing his PhD he worked as a consultant for Telefonica R&D (Barcelona). Tomico joined Eindhoven University of Technology (TU/e) in 2007 as Assistant Professor. He has been a guest researcher and lecturer at AUT Creative technologies (New Zealand), at TaiwanTech (Taiwan), Swedish School of Textiles (Sweden), Institute of Advanced Architecture (Spain), University of Tsukuba, Aalto (Finland) to name a few. During his sabbatical in 2015 he worked as a consultant for the functional textiles department at EURECAT (Spain). He recently (2017) became the head of the Industrial Design Bachelor’s degree program at ELISAVA University School of Design and Engineering of Barcelona.